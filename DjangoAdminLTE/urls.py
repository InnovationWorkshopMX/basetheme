from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib import admin
from DjangoAdminLTE import settings

urlpatterns = patterns('',
                       url(r'^$', 'DjangoAdminLTE.views.home', name='home'),
                       url(r'^login/$', 'DjangoAdminLTE.views.user_login', name='login'),
                       url(r'^logout/$', 'DjangoAdminLTE.views.user_logout', name='logout'),
                       url(r'^register/$', 'DjangoAdminLTE.views.user_register', name='register_user'),
                       url(r'^admin/', include(admin.site.urls)),

                       (r'^static/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.STATIC_ROOT}),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                       )
